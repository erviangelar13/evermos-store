module store

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/jackc/pgx/v4 v4.14.1 // indirect
	github.com/lib/pq v1.10.4 // indirect
	github.com/pilu/config v0.0.0-20131214182432-3eb99e6c0b9a // indirect
	github.com/pilu/fresh v0.0.0-20190826141211-0fa698148017 // indirect
	github.com/procyon-projects/chrono v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838 // indirect
	gopkg.in/validator.v2 v2.0.0-20210331031555-b37d688a7fb0 // indirect
	gorm.io/driver/postgres v1.2.3 // indirect
	gorm.io/gorm v1.22.5 // indirect
)
