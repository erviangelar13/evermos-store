1. Describe what you think happened that caused those bad reviews during our 12.12 event and why it happened.

    * First bad review is happened because reserve stok product when order and payment process in the system has a delay transaction on database updating data and no queue process from one and another trasaction so stock will have negative value than make a department proccess order cannot process order for customer
    * Second maybe for order checkout not always check aviability stok for next payment progress
    * Third maybe for query without db transaction, select then insert data and rollback if failure

2. Solution
    * For prevent race condition usualy using db transaction for updating data and it will be locked so it will always given last value for next transaction
    * For stock availability make sure always check stok berfore add to cart or order product, and order will reserve stok until payment process done or expired time overdue usulay in 1 hourse for waiting payment, if time overdue stok will be given back to product stock
    * For cart if stock not avaible make sure update status to pre order or item cannot be proccess 