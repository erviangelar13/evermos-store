package model

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Id      int `gorm:"autoIncrement;primaryKey"`
	Name    string
	Cart    []Cart    `gorm:"references:Id"`
	Order   []Order   `gorm:"references:Id"`
	Payment []Payment `gorm:"references:Id"`
}
