package model

import "gorm.io/gorm"

type Cart struct {
	gorm.Model
	Id        uint32 `gorm:"autoIncrement;primaryKey"`
	UserId    int
	User      User `gorm:"foreignKey:UserId;references:Id" json:"-"`
	ProductId int
	Product   Product `gorm:"foreignKey:ProductId;references:Id" json:"-"`
	Qty       int32
}
