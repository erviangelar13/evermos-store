package model

import "gorm.io/gorm"

type Product struct {
	gorm.Model
	Id      uint `gorm:"autoIncrement;primaryKey"`
	Name    string
	Price   int32
	StockId int
	Stock   Stock `gorm:"foreignKey:StockId;references:Id"`
}
