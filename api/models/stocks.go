package model

import "gorm.io/gorm"

type Stock struct {
	gorm.Model
	Id  uint32 `gorm:"primaryKey"`
	Qty int32
}
