package model

import (
	"time"

	"gorm.io/gorm"
)

type Payment struct {
	gorm.Model
	Id        uint32  `gorm:"autoIncrement;primaryKey"`
	Order     []Order `gorm:"foreignKey:PaymentId;references:Id"`
	UserId    int
	User      User `gorm:"references:ID"`
	InvoiceNo string
	PaidDate  *time.Time
	Total     int32
	Status    string
	Via       string
}
