package model

import (
	"time"

	"gorm.io/gorm"
)

type Order struct {
	gorm.Model
	Id         uint32 `gorm:"autoIncrement;primaryKey"`
	OrderNo    string
	UserId     int
	User       User `gorm:"references:Id"`
	ProductId  int
	Product    Product `gorm:"foreignKey:ProductId;references:Id"`
	PaymentId  int
	Payment    Payment `gorm:"references:Id"`
	Date       time.Time
	Expired    time.Time
	Status     string
	Qty        int32
	Price      int32
	TotalPrice int32
}
