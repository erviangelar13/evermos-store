package users

import (
	"encoding/json"
	"fmt"
	"net/http"
	model "store/api/models"
)

func (h handler) GetUsers(w http.ResponseWriter, r *http.Request) {
	var user []model.User

	if result := h.DB.Find(&user); result.Error != nil {
		fmt.Println(result.Error)
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		var response = map[string]interface{}{"status": "FAILED"}
		response["message"] = result.Error
		response["data"] = nil
		json.NewEncoder(w).Encode(response)
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	var response = map[string]interface{}{"status": "OK"}
	response["message"] = ""
	response["data"] = user
	json.NewEncoder(w).Encode(response)
}
