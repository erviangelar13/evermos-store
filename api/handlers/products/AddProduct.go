package product

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	model "store/api/models"
)

func (h handler) AddProduct(w http.ResponseWriter, r *http.Request) {
	// Read to request body
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatalln(err)
	}

	var product model.Product

	w.Header().Add("Content-Type", "application/json")

	if err := json.Unmarshal(body, &product); err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)

		var response = map[string]interface{}{"status": "FAILED"}
		response["message"] = err
		response["data"] = nil
		json.NewEncoder(w).Encode(response)
	}

	if result := h.DB.Create(&product); result.Error != nil {
		fmt.Println(result.Error)
		w.WriteHeader(http.StatusInternalServerError)

		var response = map[string]interface{}{"status": "FAILED"}
		response["message"] = err
		response["data"] = nil
		json.NewEncoder(w).Encode(response)
	}

	w.WriteHeader(http.StatusCreated)
	var response = map[string]interface{}{"status": "OK"}
	response["message"] = "Product Created"
	response["data"] = nil
	json.NewEncoder(w).Encode(response)
}
