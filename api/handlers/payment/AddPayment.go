package payment

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	model "store/api/models"
	"store/helper"
	"time"

	"gopkg.in/validator.v2"
	"gorm.io/gorm"
)

type PaymentRequest struct {
	UserId    int    `validate:"nonzero"`
	InvoiceNo string `validate:"nonzero"`
}

func (h handler) AddPayment(w http.ResponseWriter, r *http.Request) {
	// Read to request body
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatalln(err)
	}
	var payment PaymentRequest
	json.Unmarshal(body, &payment)
	fmt.Println(payment)

	if errs := validator.Validate(payment); errs != nil {
		fmt.Println(errs)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var user model.User
	if errs := h.DB.First(&user, payment.UserId).Error; errs != nil {
		fmt.Println(errs)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	var mpayment model.Payment
	// h.DB.Where("invoice_no = ?", payment.InvoiceNo).First(&mpayment)
	// fmt.Println(mpayment)

	if errs := h.DB.Preload("Order").Where("invoice_no = ?", payment.InvoiceNo).First(&mpayment).Error; errs != nil {
		fmt.Println(errs)
		if gorm.ErrRecordNotFound == errs {
			w.WriteHeader(http.StatusInternalServerError)
			response := helper.HandleErr(w, "transaction not found")
			json.NewEncoder(w).Encode(response)
			return
		}
	}
	if mpayment.Order[len(mpayment.Order)-1].Expired.After(time.Now()) && mpayment.Order[len(mpayment.Order)-1].Status == "Submited" {
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, "Failed process payment, Date Was Overdue")
		json.NewEncoder(w).Encode(response)
		return
	}
	var t = time.Now()
	mpayment.PaidDate = &t
	mpayment.Status = "Paid"
	for _, order := range mpayment.Order {
		order.Status = "Paid"
		h.DB.Exec("UPDATE orders SET status = ? WHERE id = ?;", "Paid", order.Id)
	}
	if result := h.DB.Save(&mpayment); result.Error != nil {
		fmt.Println(result.Error)
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, "Failed process payment")
		json.NewEncoder(w).Encode(response)
		return
	}
	w.WriteHeader(http.StatusCreated)
	response := helper.HandleSuccess(w, "Succes process payment", nil)
	json.NewEncoder(w).Encode(response)

}
