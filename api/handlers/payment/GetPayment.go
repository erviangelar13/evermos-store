package payment

import (
	"encoding/json"
	"fmt"
	"net/http"
	model "store/api/models"
	"store/helper"
	"time"
)

type PaymentResponse struct {
	Id        uint32
	Orders    []model.Order
	UserId    int
	UserName  string
	InvoiceNo string
	PaidDate  *time.Time
	Total     int32
	Status    string
	Via       string
}

func (h handler) GetPayment(w http.ResponseWriter, r *http.Request) {
	var payments []model.Payment

	w.Header().Add("Content-Type", "application/json")

	if result := h.DB.Find(&payments); result.Error != nil {
		fmt.Println(result.Error)
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, result.Error.Error())
		json.NewEncoder(w).Encode(response)
		return
	}
	var items []PaymentResponse
	for _, payment := range payments {
		var i = PaymentResponse{
			Id:        payment.Id,
			InvoiceNo: payment.InvoiceNo,
			UserId:    payment.UserId,
			UserName:  payment.User.Name,
			Orders:    payment.Order,
			PaidDate:  payment.PaidDate,
			Total:     payment.Total,
			Status:    payment.Status,
			Via:       payment.Via,
		}
		items = append(items, i)
	}
	w.WriteHeader(http.StatusOK)
	response := helper.HandleSuccess(w, "", items)
	json.NewEncoder(w).Encode(response)
}
