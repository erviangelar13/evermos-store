package cart

import (
	"encoding/json"
	"fmt"
	"net/http"
	model "store/api/models"
	"store/helper"
	"time"
)

type CartResponse struct {
	Id          uint32
	UserId      int
	UserName    string
	ProductId   int
	ProductName string
	Qty         int32
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func (h handler) GetCarts(w http.ResponseWriter, r *http.Request) {
	var carts []model.Cart

	w.Header().Add("Content-Type", "application/json")

	if result := h.DB.Preload("User").Preload("Product").Find(&carts); result.Error != nil {
		fmt.Println(result.Error)
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, result.Error.Error())
		json.NewEncoder(w).Encode(response)
		return
	}
	var items []CartResponse
	for _, cart := range carts {
		var i = CartResponse{
			Id:          cart.Id,
			UserId:      cart.UserId,
			UserName:    cart.User.Name,
			ProductId:   cart.ProductId,
			ProductName: cart.Product.Name,
			Qty:         cart.Qty,
			CreatedAt:   cart.CreatedAt,
			UpdatedAt:   cart.UpdatedAt,
		}
		items = append(items, i)
	}
	w.WriteHeader(http.StatusOK)
	response := helper.HandleSuccess(w, "", items)
	json.NewEncoder(w).Encode(response)
}
