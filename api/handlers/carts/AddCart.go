package cart

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	model "store/api/models"

	"store/helper"

	validator "gopkg.in/validator.v2"
	"gorm.io/gorm"
)

type CartRequest struct {
	UserId    int   `validate:"nonzero"`
	ProductId int   `validate:"nonzero"`
	Qty       int32 `validate:"min=1"`
}

func (h handler) AddCart(w http.ResponseWriter, r *http.Request) {
	// Read to request body
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatalln(err)
	}
	var cart CartRequest
	json.Unmarshal(body, &cart)
	fmt.Println(cart)

	if errs := validator.Validate(cart); errs != nil {
		fmt.Println(errs)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var user model.User
	if errs := h.DB.First(&user, cart.UserId).Error; errs != nil {
		fmt.Println(errs)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var product model.Product
	if errs := h.DB.Preload("Stock").First(&product, cart.ProductId).Error; errs != nil {
		fmt.Println(errs)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	var response = map[string]interface{}{}

	var carts model.Cart
	if errs := h.DB.Where("user_id = ? AND product_id = ? ", cart.UserId, cart.ProductId).First(&carts).Error; errs != nil {

		if product.Stock.Qty == 0 || product.Stock.Qty < cart.Qty {
			w.WriteHeader(http.StatusBadRequest)
			response := helper.HandleErr(w, "Product Stock Not Aviable")
			json.NewEncoder(w).Encode(response)
			return
		}

		if gorm.ErrRecordNotFound == errs {
			fmt.Println(cart)
			if result := h.DB.Create(&model.Cart{UserId: cart.UserId, ProductId: cart.ProductId, Qty: cart.Qty}); result.Error != nil {
				fmt.Println(result.Error)
				w.WriteHeader(http.StatusInternalServerError)
				response := helper.HandleErr(w, "Failed Add Item to Cart")
				json.NewEncoder(w).Encode(response)
				return
			}
		} else {
			carts.Qty = carts.Qty + cart.Qty
			if result := h.DB.Save(&carts); result.Error != nil {
				fmt.Println(result.Error)
				w.WriteHeader(http.StatusInternalServerError)
				response := helper.HandleErr(w, "Failed Add Item to Cart")
				json.NewEncoder(w).Encode(response)
				return
			}
		}
	}
	w.WriteHeader(http.StatusCreated)
	response["status"] = "OK"
	response["message"] = "Success Add Item to Cart"
	response["data"] = nil
	json.NewEncoder(w).Encode(response)

}
