package order

import (
	"fmt"
	model "store/api/models"
	"time"
)

func (h handler) CancleOrder() {
	fmt.Println("Calling Cancle")

	tx := h.DB.Begin()
	var orders []model.Order
	if err := tx.Preload("Product").Where("expired < ? and status = 'Submited'", time.Now()).Find(&orders).Error; err != nil {
		fmt.Println(err)
		tx.Rollback()
		return
	}
	// fmt.Println(orders)
	for _, order := range orders {
		var qty int
		if err := tx.Raw("SELECT qty FROM stocks WHERE id = ? FOR UPDATE;", order.Product.StockId).Scan(&qty).Error; err != nil {
			fmt.Println(err)
			tx.Rollback()
			return
		}

		if err := tx.Exec("UPDATE stocks SET qty = ? WHERE id = ?;", qty+int(order.Qty), order.Product.StockId).Error; err != nil {
			fmt.Println(err)
			tx.Rollback()
			return
		}

		if err := tx.Exec("UPDATE orders SET status = ? WHERE id = ?;", "Cancled", order.Id).Error; err != nil {
			fmt.Println(err)
			tx.Rollback()
			return
		}
	}

	if err := tx.Commit().Error; err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Calling Cancle Done")
}
