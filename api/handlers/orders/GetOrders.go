package order

import (
	"encoding/json"
	"fmt"
	"net/http"
	model "store/api/models"
	"store/helper"
	"time"
)

type OrderResponse struct {
	Id            uint32
	UserId        int
	UserName      string
	ProductId     int
	ProductName   string
	InvoiceNo     string
	StatusPayment string
	Date          time.Time
	Expired       time.Time
	Qty           int32
	Price         int32
	TotalPrice    int32
	CreatedAt     time.Time
	UpdatedAt     time.Time
}

func (h handler) GetOders(w http.ResponseWriter, r *http.Request) {
	var orders []model.Order

	w.Header().Add("Content-Type", "application/json")

	if result := h.DB.Preload("Product").Preload("User").Preload("Payment").Find(&orders); result.Error != nil {
		fmt.Println(result.Error)
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, result.Error.Error())
		json.NewEncoder(w).Encode(response)
		return
	}
	var items []OrderResponse
	for _, order := range orders {
		var i = OrderResponse{
			Id:            order.Id,
			UserId:        order.UserId,
			UserName:      order.User.Name,
			ProductId:     order.ProductId,
			ProductName:   order.Product.Name,
			Qty:           order.Qty,
			Price:         order.Price,
			TotalPrice:    order.TotalPrice,
			Date:          order.Date,
			Expired:       order.Expired,
			InvoiceNo:     order.Payment.InvoiceNo,
			StatusPayment: order.Payment.Status,
			CreatedAt:     order.CreatedAt,
			UpdatedAt:     order.UpdatedAt,
		}
		items = append(items, i)
	}
	w.WriteHeader(http.StatusOK)
	response := helper.HandleSuccess(w, "", items)
	json.NewEncoder(w).Encode(response)
}
