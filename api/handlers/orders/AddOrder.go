package order

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	model "store/api/models"
	"time"

	"store/helper"

	validator "gopkg.in/validator.v2"
)

type OrderRequest struct {
	UserId    int   `validate:"nonzero"`
	ProductId int   `validate:"nonzero"`
	Qty       int32 `validate:"min=1"`
	Provider  int   `validate:"nonzero"`
}

type PaymentVia struct {
	Id       int
	name     string
	provider string
}

func (h handler) AddOrder(w http.ResponseWriter, r *http.Request) {
	// Read to request body
	var via = []PaymentVia{
		{
			Id:       1,
			name:     "Virtual Account",
			provider: "BCA",
		},
		{

			Id:       2,
			name:     "E-Wallet",
			provider: "Gopay",
		},
	}
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatalln(err)
	}
	var order OrderRequest
	json.Unmarshal(body, &order)
	// fmt.Println(order)

	w.Header().Add("Content-Type", "application/json")
	if errs := validator.Validate(order); errs != nil {
		fmt.Println(errs)
		w.WriteHeader(http.StatusBadRequest)
		response := helper.HandleErr(w, errs.Error())
		json.NewEncoder(w).Encode(response)
		return
	}

	var user model.User
	if errs := h.DB.First(&user, order.UserId).Error; errs != nil {
		fmt.Println(errs)
		w.WriteHeader(http.StatusBadRequest)
		response := helper.HandleErr(w, errs.Error())
		json.NewEncoder(w).Encode(response)
		return
	}

	var product model.Product
	if errs := h.DB.Preload("Stock").First(&product, order.ProductId).Error; errs != nil {
		fmt.Println(errs)
		w.WriteHeader(http.StatusBadRequest)
		response := helper.HandleErr(w, errs.Error())
		json.NewEncoder(w).Encode(response)
		return
	}

	if product.Stock.Qty == 0 || product.Stock.Qty < order.Qty {
		w.WriteHeader(http.StatusBadRequest)
		response := helper.HandleErr(w, "Product Stock Not Aviable")
		json.NewEncoder(w).Encode(response)
		return
	}

	tx := h.DB.Begin()

	confMap := map[string]string{}
	for _, v := range via {
		if v.Id == order.Provider {
			confMap["name"] = v.name
		}
	}
	var provider string
	if v, ok := confMap["name"]; ok {
		provider = v
	}

	if err := tx.Create(&model.Order{
		OrderNo:    helper.GenerateSequence("ODR", 10, tx),
		UserId:     order.UserId,
		ProductId:  order.ProductId,
		Date:       time.Now(),
		Expired:    time.Now().Add(time.Hour * time.Duration(1)),
		Qty:        order.Qty,
		Price:      product.Price,
		TotalPrice: product.Price * order.Qty,
		Status:     "Submited",
		Payment: model.Payment{
			UserId:    order.UserId,
			InvoiceNo: helper.GenerateInvoice("INV", 10, tx),
			Total:     product.Price * order.Qty,
			Status:    "Waiting Payment",
			Via:       provider,
			PaidDate:  nil,
		},
	}).Error; err != nil {
		tx.Rollback()
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, "Failed Submited Order")
		json.NewEncoder(w).Encode(response)
		return
	}

	var qty int
	if err := tx.Raw("SELECT qty FROM stocks WHERE id = ? FOR UPDATE;", product.StockId).Scan(&qty).Error; err != nil {
		tx.Rollback()
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, err.Error())
		json.NewEncoder(w).Encode(response)
		return
	}

	if err := tx.Exec("UPDATE stocks SET qty = ? WHERE id = ?;", qty-int(order.Qty), product.StockId).Error; err != nil {
		tx.Rollback()
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, err.Error())
		json.NewEncoder(w).Encode(response)
		return
	}

	if err := tx.Commit().Error; err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		response := helper.HandleErr(w, "Failed Submited Order")
		json.NewEncoder(w).Encode(response)
		return
	}
	w.WriteHeader(http.StatusCreated)
	response := helper.HandleSuccess(w, "Success Submited Order", nil)
	json.NewEncoder(w).Encode(response)

}
