package helper

import (
	"fmt"
	"strconv"
	"time"

	"gorm.io/gorm"
)

func GenerateSequence(prefix string, maxlength int, tx *gorm.DB) string {
	t := time.Now()
	var result int64
	tx.Table("orders").Count(&result)
	var lastId = int(result + 1)
	var seq = prefix + "-" + fmt.Sprintf("%d%02d", t.Year(), t.Month())

	for i := 0; i < (maxlength - len(prefix) - len(strconv.Itoa(lastId))); i++ {
		seq = fmt.Sprintf("%s", seq+"0")
	}
	return seq + strconv.Itoa(lastId)
}

func GenerateInvoice(prefix string, maxlength int, tx *gorm.DB) string {
	t := time.Now()
	var result int64
	tx.Table("payments").Count(&result)
	var lastId = int(result + 1)
	var seq = prefix + "/" + fmt.Sprintf("%d%02d", t.Year(), t.Month()) + "/"

	for i := 0; i < (maxlength - len(prefix) - len(strconv.Itoa(lastId))); i++ {
		seq = fmt.Sprintf("%s", seq+"0")
	}
	return seq + strconv.Itoa(lastId)
}
