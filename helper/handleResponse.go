package helper

import (
	"net/http"
	"time"
)

func HandleErr(w http.ResponseWriter, message string) map[string]interface{} {
	var response = map[string]interface{}{}
	response["status"] = "FAILED"
	response["message"] = message
	response["timestamp"] = time.Now()
	return response
}

func HandleSuccess(w http.ResponseWriter, message string, data interface{}) map[string]interface{} {
	var response = map[string]interface{}{}
	response["status"] = "OK"
	response["message"] = message
	response["data"] = data
	response["timestamp"] = time.Now()
	return response
}
