package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type Config struct {
	Environment      string
	ConnectionString string
}

func Setup(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	datas := strings.Split(string(data), "\n")
	for _, env := range datas {
		e := strings.Split(env, "=")
		if len(e) >= 2 {
			os.Setenv(strings.TrimSpace(e[0]), strings.TrimSpace(strings.Join(e[1:], "=")))
		}
	}

	return nil
}

func getConfigValue(envName string, defaultValue string) string {
	if val, ok := os.LookupEnv(envName); ok {
		return val
	}

	return defaultValue
}

func NewConfig() *Config {
	return &Config{
		Environment: getConfigValue("ENV", "local"),
		ConnectionString: fmt.Sprintf(
			"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			getConfigValue("POSTGRES_HOST", "localhost"), getConfigValue("POSTGRES_PORT", "5432"), getConfigValue("POSTGRES_USER", ""),
			getConfigValue("POSTGRES_PASSWORD", ""), getConfigValue("POSTGRES_DB", ""),
		),
	}
}
