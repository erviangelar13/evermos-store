package db

import (
	"log"

	model "store/api/models"
	"store/config"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

//Connect PostgreSQL DB and Migration Model
func ConnectDB() *gorm.DB {
	host := config.NewConfig().ConnectionString
	db, err := gorm.Open(postgres.Open(host), &gorm.Config{})
	if err != nil {
		log.Fatalln(err)
	}
	db.AutoMigrate(&model.User{}, &model.Product{}, &model.Stock{}, &model.Cart{}, &model.Order{}, &model.Payment{})
	return db
}
