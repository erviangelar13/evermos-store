package seed

import (
	model "store/api/models"

	"gorm.io/gorm"
)

type Seed struct {
	Name string
	Run  func(*gorm.DB) error
}

func CreateUser(db *gorm.DB, name string) error {
	var user model.User
	if err := db.Where("name = ?", name).First(&user).Error; err != nil {
		if gorm.ErrRecordNotFound == err {
			return db.Create(&model.User{Name: name}).Error
		}
	}
	return nil
}

func CreateProduct(db *gorm.DB, name string, price int, qty int) error {
	var product model.Product
	if result := db.Where("name = ?", name).First(&product); result != nil {
		if gorm.ErrRecordNotFound == result.Error {
			return db.Create(&model.Product{Name: name, Price: int32(price), Stock: model.Stock{Qty: int32(qty)}}).Error
		}
	}
	return nil
}

func All() []Seed {
	return []Seed{
		{
			Name: "User 1",
			Run: func(db *gorm.DB) error {
				return CreateUser(db, "Jane")
			},
		},
		{
			Name: "User 2",
			Run: func(db *gorm.DB) error {
				return CreateUser(db, "John")
			},
		},
		{
			Name: "Product 1",
			Run: func(db *gorm.DB) error {
				return CreateProduct(db, "Product 1", 500000, 10)
			},
		},
		{
			Name: "Product 2",
			Run: func(db *gorm.DB) error {
				return CreateProduct(db, "Product 2", 150000, 20)
			},
		},
		{
			Name: "Product 3",
			Run: func(db *gorm.DB) error {
				return CreateProduct(db, "Product 3", 7500000, 7)
			},
		},
	}
}

// func CreateUser(db *gorm.DB, name string, age int) error {
// 	return db.Create(& {Name: name, Age: age}).Error
// }
