package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	cart "store/api/handlers/carts"
	order "store/api/handlers/orders"
	payment "store/api/handlers/payment"
	product "store/api/handlers/products"
	"store/api/handlers/users"
	"store/config"
	"store/config/db"
	"store/config/seed"
	"time"

	"github.com/gorilla/mux"
	"github.com/procyon-projects/chrono"
)

func main() {
	log.Println("starting api server")

	//Check Active Environment if Not Exist Setup environment bassed .env file
	if _, ok := os.LookupEnv("PORT"); !ok {
		config.Setup(".env")
	}

	db := db.ConnectDB()
	router := mux.NewRouter()

	//Router For Seeding using api
	router.HandleFunc("/seed", func(w http.ResponseWriter, r *http.Request) {
		for _, seed := range seed.All() {
			if err := seed.Run(db); err != nil {
				log.Fatalf("Running seed '%s', failed with error: %s", seed.Name, err)
			}
		}
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		var response = map[string]interface{}{"status": "OK"}
		response["message"] = "success seeding data"
		json.NewEncoder(w).Encode(response)
	})

	router.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode("Welcome")
	})

	h := users.New(db)
	router.HandleFunc("/users", h.GetUsers).Methods(http.MethodGet)
	router.HandleFunc("/user", h.GetUsers).Methods(http.MethodPost)

	p := product.New(db)
	router.HandleFunc("/products", p.GetProducts).Methods(http.MethodGet)
	router.HandleFunc("/product", p.AddProduct).Methods(http.MethodPost)

	c := cart.New(db)
	router.HandleFunc("/carts", c.GetCarts).Methods(http.MethodGet)
	router.HandleFunc("/cart", c.AddCart).Methods(http.MethodPost)

	o := order.New(db)
	router.HandleFunc("/orders", o.GetOders).Methods(http.MethodGet)
	router.HandleFunc("/order", o.AddOrder).Methods(http.MethodPost)

	py := payment.New(db)
	router.HandleFunc("/payments", py.GetPayment).Methods(http.MethodGet)
	router.HandleFunc("/payment", py.AddPayment).Methods(http.MethodPost)

	//Schedule For Check Expired Order And Cancle Reserve Stock
	taskScheduler := chrono.NewDefaultTaskScheduler()
	_, err := taskScheduler.ScheduleAtFixedRate(func(ctx context.Context) {
		log.Print("Fixed Rate of 5 Second")
		o.CancleOrder()
	}, 10*time.Second)

	if err == nil {
		log.Print("Task has been scheduled successfully.")
	}

	port := os.Getenv("PORT")
	log.Println(port)
	if err := http.ListenAndServe(fmt.Sprintf(":%s", port), router); err != nil {
		log.Fatalf("error: listening and serving: %s", err)
	}
}
